import React, { useEffect } from "react";
import * as WebBrowser from "expo-web-browser";
import * as AuthSession from 'expo-auth-session';
const authorizationEndpoint = `https://auth-dev.exelatech.com/realms/ConsumerRealm/protocol/openid-connect/auth`;
const NATIVE_APP_URL = 'https://web-567248525f4d726567.am.os.inventr.ai'
WebBrowser.maybeCompleteAuthSession();

export default function KCLoginComponent({ onSuccess, source, message, clientId }) {
  // WebBrowser.WebBrowserPresentationStyle.OVER_FULL_SCREEN;
  let extraParams = { application_url: encodeURIComponent(NATIVE_APP_URL), platform: "native"};
  if (source === "changePwd") //In case change Password
    extraParams = { ...extraParams, ...{ kc_action: "UPDATE_PASSWORD" } };
  if (!!message) // Use custom error message
    extraParams = { ...extraParams, ...{ error_message: message } };

    // native: 'https://auth.expo.io/@exela/XBP'
  const redirectUri = AuthSession.makeRedirectUri({
   scheme: "com.xbpclaims.global"
  });

  const config = {
    clientId: clientId,
    redirectUri,
    usePKCE: false,
    extraParams: extraParams,
    scopes: ['openid', "profile"],  //"offline_access"
  };

  // Request
  const [request, response, promptAsync] = AuthSession.useAuthRequest(config, {
    authorizationEndpoint: authorizationEndpoint
  });

  console.log('request ', request)
  useEffect(() => {
    if (!!request)
      promptAsync();
  }, [promptAsync]);

  useEffect(() => {
    if (!!response) {
      response.redirectUri = request.redirectUri
      response.clientId = request.clientId,
      response.scopes = request.scopes
      onSuccess(response);
      return () => {
        console.log("This will be logged on unmount");
      }
    }
  }, [response]);

  return (
    <></>
  );
}