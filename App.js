import * as React from 'react';
import * as WebBrowser from 'expo-web-browser';
import { makeRedirectUri, useAuthRequest, useAutoDiscovery } from 'expo-auth-session';
import { Button, Text, View } from 'react-native';
import * as Linking from 'expo-linking';

WebBrowser.maybeCompleteAuthSession();

export default function App() {
  const discovery = useAutoDiscovery('https://auth-dev.exelatech.com/realms/ConsumerRealm');

// Create and load an auth request
  const [request, result, promptAsync] = useAuthRequest(
    {
      clientId: 'xbppayer',
      redirectUri: makeRedirectUri({
        // invalid redirect uri
        // scheme: 'com.amitrathee729.expo48'

        // getting redirection after login
        scheme: Linking.createURL("com.amitrathee729.expo48://"),
        // useProxy: true
        // invalid redirect uri
        // scheme: Linking.createURL("com.test.expo://")
      }),
      scopes: ['openid', 'profile'],
      usePKCE: true,
    },
    discovery
  );

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Button title="Login!" disabled={!request} onPress={() => promptAsync()} />
      {result && <Text>{JSON.stringify(result, null, 2)}</Text>}
    </View>
  );
}



// import React, { useEffect } from "react";
// import * as WebBrowser from "expo-web-browser";
// import * as AuthSession from 'expo-auth-session';
// const authorizationEndpoint = `https://auth-dev.exelatech.com/realms/ConsumerRealm/protocol/openid-connect/auth`;
// const NATIVE_APP_URL = 'https://web-567248525f4d726567.am.os.inventr.ai'
// WebBrowser.maybeCompleteAuthSession();

// export default function App() {
//   // WebBrowser.WebBrowserPresentationStyle.OVER_FULL_SCREEN;
//   let extraParams = { application_url: encodeURIComponent(NATIVE_APP_URL), platform: "native"};


//     // native: 'https://auth.expo.io/@exela/XBP'
//   const redirectUri = AuthSession.makeRedirectUri({
//    scheme: "com.xbpclaims.global"
//   });

//   const config = {
//     clientId: 'xbppayer',
//     redirectUri,
//     usePKCE: false,
//     extraParams: extraParams,
//     scopes: ['openid', "profile"],  //"offline_access"
//   };

//   // Request
//   const [request, response, promptAsync] = AuthSession.useAuthRequest(config, {
//     authorizationEndpoint: authorizationEndpoint
//   });


//   useEffect(() => {
//     if (!!response) {
//       response.redirectUri = request.redirectUri
//       response.clientId = request.clientId,
//       response.scopes = request.scopes
//       // onSuccess(response);
//       alert('success')
//       return () => {
//         console.log("This will be logged on unmount");
//       }
//     }
//   }, [response]);

//   return (
//           <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//             <Button title="Login!" disabled={!request} onPress={() => promptAsync()} />
//             {response && <Text>{JSON.stringify(response, null, 2)}</Text>}
//           </View>
//   );
// }

// import {
//   ActivityIndicator,
//   Button,
//   ScrollView,
//   Text,
//   View,
// } from "react-native";
// import * as AuthSession from "expo-auth-session";
// import * as WebBrowser from "expo-web-browser";
// import { useEffect, useState } from "react";

// WebBrowser.maybeCompleteAuthSession();
// const redirectUri = AuthSession.makeRedirectUri({
//   useProxy: false,
//   scheme: Linking.createURL("com.amitrathee729.expo48://")
// });

// // Keycloak details
// const keycloakUri = "https://auth-dev.exelatech.com";
// const keycloakRealm = "ConsumerRealm";
// const clientId = "xbppayer";

// export function generateShortUUID() {
//   return Math.random().toString(36).substring(2, 15);
// }

// export default function App() {
//   const [accessToken, setAccessToken] = useState("");
//   const [idToken, setIdToken] = useState("");
//   const [refreshToken, setRefreshToken] = useState("");
//   const [discoveryResult, setDiscoveryResult] =
//     useState();

//   // Fetch OIDC discovery document once
//   useEffect(() => {
//     const getDiscoveryDocument = async () => {
//       const discoveryDocument = await AuthSession.fetchDiscoveryAsync(
//         `${keycloakUri}/realms/${keycloakRealm}`
//       );
//       setDiscoveryResult(discoveryDocument);
//     };
//     getDiscoveryDocument();
//   }, []);

//   const login = async () => {
//     const state = generateShortUUID();
//     // Get Authorization code
//     const authRequestOptions = {
//       responseType: AuthSession.ResponseType.Code,
//       clientId,
//       redirectUri: redirectUri,
//       prompt: AuthSession.Prompt.Login,
//       state: state,
//       usePKCE: true,
//     };
//     const authRequest = new AuthSession.AuthRequest(authRequestOptions);
//     const authorizeResult = await authRequest.promptAsync(discoveryResult, {
//       useProxy: false,
//     });
//     alert(JSON.stringify(authorizeResult))
//     if (authorizeResult.type === "success") {
//       // If successful, get tokens
//       const tokenResult = await AuthSession.exchangeCodeAsync(
//         {
//           code: authorizeResult.params.code,
//           clientId: clientId,
//           redirectUri: redirectUri,
//           extraParams: {
//             code_verifier: authRequest.codeVerifier || "",
//           },
//         },
//         discoveryResult
//       );

//       setAccessToken(tokenResult.accessToken);
//       setIdToken(tokenResult.idToken);
//       setRefreshToken(tokenResult.refreshToken);
//     }
//   };

//   const refresh = async () => {
//     const refreshTokenObject = {
//       clientId: clientId,
//       refreshToken: refreshToken,
//     };
//     const tokenResult = await AuthSession.refreshAsync(
//       refreshTokenObject,
//       discoveryResult
//     );

//     setAccessToken(tokenResult.accessToken);
//     setIdToken(tokenResult.idToken);
//     setRefreshToken(tokenResult.refreshToken);
//   };

//   const logout = async () => {
//     if (!accessToken) return;
//     const redirectUrl = AuthSession.makeRedirectUri({ useProxy: false });
//     const revoked = await AuthSession.revokeAsync(
//       { token: accessToken },
//       discoveryResult
//     );
//     if (!revoked) return;

//     // The default revokeAsync method doesn't work for Keycloak, we need to explicitely invoke the OIDC endSessionEndpoint with the correct parameters
//     const logoutUrl = `${discoveryResult
//       .endSessionEndpoint}?client_id=${clientId}&post_logout_redirect_uri=${redirectUrl}&id_token_hint=${idToken}`;

//     const res = await WebBrowser.openAuthSessionAsync(logoutUrl, redirectUrl);
//     if (res.type === "success") {
//       setAccessToken(undefined);
//       setIdToken(undefined);
//       setRefreshToken(undefined);
//     }
//   };

//   if (!discoveryResult) return <ActivityIndicator />;

//   return (
//     <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
//       {refreshToken ? (
//         <View
//           style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
//         >
//           <View>
//             <ScrollView style={{ flex: 1 }}>
//               <Text>AccessToken: {accessToken}</Text>
//               <Text>idToken: {idToken}</Text>
//               <Text>refreshToken: {refreshToken}</Text>
//             </ScrollView>
//           </View>
//           <View>
//             <Button title="Refresh" onPress={refresh} />
//             <Button title="Logout" onPress={logout} />
//           </View>
//         </View>
//       ) : (
//         <Button title="Login" onPress={login} />
//       )}
//     </View>
//   );
// }